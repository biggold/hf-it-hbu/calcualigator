# CalcuAligator
CalcuAligator is a thiny calculator capable of many different calculations.
It can calculate any combination of dot and dash operations in any order. It also supports negative numbers if you like to calculate with them.
## Installation
Should work in Intellij and Eclipse if imported as Maven Project.
Main method can be found under (./src/main/java | .\src\main\java)

## Notes

* Output file can be found under (./src/main/resources | .\src\main\resources)
