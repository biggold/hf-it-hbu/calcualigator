package models;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class VolumeTest {

    Volume v = new Volume();

    @Test
    void volumeCalculate() {
        v.setHeight(10);
        v.setDepth(20);
        v.setLength(30);
        v.calculate();

        assertEquals( 6000,v.getResult() );
    }
}