package models;

import exceptions.InvalidDelimiter;
import exceptions.InvalidOperator;
import exceptions.ValueCreatesInvalidExpression;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ExpressionTest {
    Expression expression = new Expression();

    @Test
    void Expression() {
        // Test empty constructor
        assertEquals("", expression.getExpression());
        // Test with initial value constructor
        double number = 12.3254d;
        assertDoesNotThrow(() -> expression = new Expression(number));
        assertEquals(Double.toString(number), expression.getExpression());
    }

    @Test
    void getComputableExpression() {
        for (char ch: "5*3+2".toCharArray()) {
            if (Expression.getAllowedOperators().contains(ch)) {
                assertDoesNotThrow(() -> expression.addOperator(ch));
            }else if(Expression.getAllowedDelimiters().contains(ch)) {
                assertDoesNotThrow(() -> expression.addDelimiter(ch));
            } else {
                assertDoesNotThrow(() -> expression.addNumber(Integer.parseInt(String.valueOf(ch))));
            }
        }
        assertEquals("5 * 3 + 2", expression.getExpression());
        List<Object> expected = new ArrayList<Object>(Arrays.asList(5d, "*", 3d, "+", 2d));
        assertEquals(expected, expression.getComputableExpression());
    }

    @Test
    void addNumber() {
        // We simulate her some key presses
        expression.addNumber(1);
        assertEquals("1", expression.getExpression());
        expression.addNumber(4);
        assertEquals("14", expression.getExpression());
    }


    @Test
    void addDelimiter() {
        // We simulate her some key presses
        assertThrows(ValueCreatesInvalidExpression.class, () -> expression.addDelimiter('.'));
        expression.addNumber(1);
        assertDoesNotThrow(() -> expression.addDelimiter('.'));
        assertEquals("1.", expression.getExpression());
    }

    @Test
    void addWrongDelimiter() {
        // We simulate her some key presses
        assertThrows(ValueCreatesInvalidExpression.class, () -> expression.addDelimiter(','));
        expression.addNumber(1);
        assertThrows(InvalidDelimiter.class, () -> expression.addDelimiter(','));
    }

    @Test
    void addOperator() {
        // We simulate her some key presses
        assertThrows(ValueCreatesInvalidExpression.class, () -> expression.addOperator('*'));
        // Test multiplication operator
        expression.addNumber(1);
        assertDoesNotThrow(() -> expression.addOperator('*'));
        assertEquals("1 * ", expression.getExpression());
        // Test division operator
        expression.addNumber(1);
        assertDoesNotThrow(() -> expression.addOperator('/'));
        assertEquals("1 * 1 / ", expression.getExpression());
        // Test addition operator
        expression.addNumber(1);
        assertDoesNotThrow(() -> expression.addOperator('+'));
        assertEquals("1 * 1 / 1 + ", expression.getExpression());
        // Test subtraction operator
        expression.addNumber(1);
        assertDoesNotThrow(() -> expression.addOperator('-'));
        assertEquals("1 * 1 / 1 + 1 - ", expression.getExpression());
        // TODO: Test - (negative number vs operator)
    }

    @Test
    void addWrongOperator() {
        // We simulate her some key presses
        assertThrows(ValueCreatesInvalidExpression.class, () -> {
            expression.addOperator('^');
        });
        expression.addNumber(1);
        assertThrows(InvalidOperator.class, () -> expression.addOperator('^'));
        // TODO: Test - (negative number vs operator)
    }

    @ParameterizedTest
    @CsvSource({"5*3+2,17d", "5*10/25,2d", "5+10-15,0d", "200*12+13/12*3+4/0.6*0.3-10,2395.25d", "0-100+10,-90d", "0.0000000021*2,4.2E-9"})
    void calculate(String calculation, Double result) {
        for (char ch: calculation.toCharArray()) {
            if (Expression.getAllowedOperators().contains(ch)) {
                assertDoesNotThrow(() -> expression.addOperator(ch));
            }else if(Expression.getAllowedDelimiters().contains(ch)) {
                assertDoesNotThrow(() -> expression.addDelimiter(ch));
            } else {
                assertDoesNotThrow(() -> expression.addNumber(Integer.parseInt(String.valueOf(ch))));
            }
        }
        // Double asserts to catch the exception if it occurs (shouldn't happen)
        assertDoesNotThrow(() -> assertEquals(result, expression.calculate()));
    }
}