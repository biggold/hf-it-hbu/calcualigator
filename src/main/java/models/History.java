package models;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.Vector;

/**
 * Data model for History View.
 *
 * @author Cyrill Näf & Nicolas Stutz
 */
public class History implements TableModel {
    private Vector<Calculation> rows = new Vector<Calculation>();
    private Vector<TableModelListener> listeners = new Vector<TableModelListener>();

    /**
     * Adds calculation to data set.
     *
     * @param calculation
     */
    public void addCalculation(Calculation calculation) {
        int index = rows.size();
        rows.add(calculation);
        TableModelEvent e = new TableModelEvent(this, index, index,
                TableModelEvent.ALL_COLUMNS, TableModelEvent.INSERT);
        updateListeners(e);
    }

    /**
     * Removes all calculations from data set.
     */
    public void removeAllCalculations() {
        rows.removeAllElements();
        TableModelEvent e = new TableModelEvent(this);
        updateListeners(e);
    }

    /**
     * Notifies all Listeners of given update.
     *
     * @param e
     */
    private void updateListeners(TableModelEvent e) {
        for (TableModelListener listener : listeners) {
            (listener).tableChanged(e);
        }
    }

    public int getColumnCount() {
        return 2;
    }

    public int getRowCount() {
        return rows.size();
    }

    /**
     * Returns the name of a column.
     *
     * @param column
     * @return
     */
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Expression";
            case 1:
                return "Result";
            default:
                return null;
        }
    }

    /**
     * Returns the object at given position.
     *
     * @param rowIndex
     * @param columnIndex
     * @return
     */
    public Object getValueAt(int rowIndex, int columnIndex) {
        Calculation calculation = (Calculation) rows.get(rowIndex);
        return switch (columnIndex) {
            case 0 -> calculation.getExpression();
            case 1 -> calculation.getResult();
            default -> null;
        };
    }

    /**
     * Returns the class of the requested column.
     *
     * @param columnIndex
     * @return
     */
    public Class getColumnClass(int columnIndex) {
        return switch (columnIndex) {
            case 0 -> String.class;
            case 1 -> Double.class;
            default -> null;
        };
    }

    /**
     * Adds a Eventlistener.
     *
     * @param l
     */
    public void addTableModelListener(TableModelListener l) {
        listeners.add(l);
    }

    /**
     * Removes a Eventlistener.
     *
     * @param l
     */
    public void removeTableModelListener(TableModelListener l) {
        listeners.remove(l);
    }

    /**
     * @param rowIndex
     * @param columnIndex
     * @return
     */
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    /**
     * Not implemented. Because allready restricted in View.
     *
     * @param aValue
     * @param rowIndex
     * @param columnIndex
     */
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        // Not implemented. Because allready restricted in View.
    }
}