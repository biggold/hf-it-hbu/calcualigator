package models;

import exceptions.InvalidDelimiter;
import exceptions.InvalidExpression;
import exceptions.InvalidOperator;
import exceptions.ValueCreatesInvalidExpression;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Expression contains the necessary functions to create a valid mathematical expression and compute it afterwards.
 *
 * @author Cyrill Näf & Nicolas Stutz
 */
public class Expression {
    private String expression = "";
    private boolean numberNeeded = true;
    private boolean negativeNumber = false;
    private boolean dotUsed = false;
    private static final List<Character> allowedDelimiters = List.of('.');
    private static final List<Character> allowedOperators = List.of('*', '/', '+', '-');

    public Expression() {
    }

    /**
     * Initializes Expression with an initial value. Useful to continue calculation after initial calculation.
     *
     * @param initialValue Value of prior calculation
     * @throws InvalidOperator
     * @throws InvalidDelimiter
     * @throws ValueCreatesInvalidExpression
     */
    public Expression(Double initialValue) throws InvalidOperator, InvalidDelimiter, ValueCreatesInvalidExpression {
        addObjectToExpression(initialValue);
    }

    /**
     * Adds any object to an Expression.
     *
     * @param obj Any object cappable to convert to string via .toString() method.
     * @throws InvalidOperator
     * @throws InvalidDelimiter
     * @throws ValueCreatesInvalidExpression
     */
    private void addObjectToExpression(Object obj) throws InvalidOperator, InvalidDelimiter, ValueCreatesInvalidExpression {
        String objString;
        if (obj instanceof Double) {
            objString = BigDecimal.valueOf((double) obj).toPlainString();
        } else {
            objString = obj.toString();
        }
        for (char ch : objString.toCharArray()) {
            if (allowedOperators.contains(ch)) {
                addOperator(ch);
            } else if (allowedDelimiters.contains(ch)) {
                addDelimiter(ch);
            } else {
                addNumber(Integer.parseInt(String.valueOf(ch)));
            }
        }
    }

    /**
     * Internal constructor, convertes computable Expression top new Expression.
     * Used for recursion.
     *
     * @param computableExpression Requires a valid computable Expression Object.
     * @throws InvalidOperator
     * @throws InvalidDelimiter
     * @throws ValueCreatesInvalidExpression
     */
    private Expression(List<Object> computableExpression) throws InvalidOperator, InvalidDelimiter, ValueCreatesInvalidExpression {
        for (Object obj : computableExpression) {
            addObjectToExpression(obj);
        }
    }

    public String getExpression() {
        return expression;
    }

    public static List<Character> getAllowedDelimiters() {
        return allowedDelimiters;
    }

    public static List<Character> getAllowedOperators() {
        return allowedOperators;
    }

    /**
     * Defines if a Epression is valid.
     *
     * @return
     */
    public Boolean isValid() {
        return !numberNeeded;
    }

    /**
     * Generats a computable Expression from object variable Expression.
     *
     * @return
     */
    List<Object> getComputableExpression() {
        String[] arrOfStr = expression.split(" ");
        List<Object> computableExpression = new ArrayList<Object>();
        for (String item : arrOfStr) {
            try {
                computableExpression.add(Double.valueOf(item));
            } catch (NumberFormatException e) {
                computableExpression.add(item);
            }
        }
        return computableExpression;
    }


    /**
     * Adds any number to the Expression.
     *
     * @param number A number 0-9
     */
    public void addNumber(int number) {
        expression = expression + number;
        numberNeeded = false;
    }

    /**
     * Adds an delimiter (of allowedDelimiters list) to the Expression.
     *
     * @param delimiter The delimiter as char like .
     * @throws ValueCreatesInvalidExpression
     * @throws InvalidDelimiter
     */
    public void addDelimiter(char delimiter) throws ValueCreatesInvalidExpression, InvalidDelimiter {
        if (numberNeeded || dotUsed) {
            throw new ValueCreatesInvalidExpression(delimiter, expression);
        } else if (allowedDelimiters.contains(delimiter) && !dotUsed) {
            expression = expression + delimiter;
            numberNeeded = true;
            dotUsed = true;
        } else if (!allowedDelimiters.contains(delimiter)) {
            throw new InvalidDelimiter(delimiter, allowedDelimiters);
        }
    }

    /**
     * Adds an operator (of allowedOperator list) to the expression.
     *
     * @param operator The operator as char like *
     * @throws ValueCreatesInvalidExpression
     * @throws InvalidOperator
     */
    public void addOperator(char operator) throws ValueCreatesInvalidExpression, InvalidOperator {
        if (numberNeeded) {
            if (operator == '-' && !negativeNumber) {
                expression = expression + operator;
                negativeNumber = true;
            } else {
                throw new ValueCreatesInvalidExpression(operator, expression);
            }
        } else if (allowedOperators.contains(operator)) {
            expression = expression + " " + operator + " ";
            numberNeeded = true;
            negativeNumber = false;
            dotUsed = false;
        } else if (!allowedOperators.contains(operator)) {
            throw new InvalidOperator(operator, allowedOperators);
        }
    }

    /**
     * Function to add two values together.
     *
     * @param a Value a
     * @param b Value b
     * @return Returns the result of the addition
     */
    private static double addition(double a, double b) {
        return a + b;
    }

    /**
     * Function to subtract one value from the other.
     *
     * @param a Value where we subtract
     * @param b Value b is subtracted from value a
     * @return Returns the result of the subtraction
     */
    private static double subtraction(double a, double b) {
        return a - b;
    }

    /**
     * Used to multiply two values with each other.
     *
     * @param a Value a
     * @param b Value b
     * @return Returns the result of the multiplication
     */
    private static double multiplication(double a, double b) {
        return a * b;
    }

    /**
     * Function to divide two values.
     *
     * @param a Divisor
     * @param b Quotient
     * @return Returns the result of the division
     */
    private static double division(double a, double b) {
        return a / b;
    }

    /**
     * Calculates the given expression in a recursive way.
     *
     * @return
     * @throws Exception         All java default exceptions which could occur!
     * @throws InvalidExpression Means that the expression is not a valid one.
     */
    public double calculate() throws InvalidExpression, Exception {
        if (isValid()) {
            List<Object> computableExpression = getComputableExpression();
            if (computableExpression.size() != 1 && isValid()) {
                for (int i = 0; i < computableExpression.size(); i++) {
                    Object currObj = computableExpression.get(i);
                    if (currObj instanceof String) {
                        String operator = currObj.toString();
                        if (operator.equals("*")) {
                            computableExpression.set(i, multiplication((double) computableExpression.get(i - 1), (double) computableExpression.get(i + 1)));
                        } else if (operator.equals("/")) {
                            computableExpression.set(i, division((double) computableExpression.get(i - 1), (double) computableExpression.get(i + 1)));
                        } else if (!computableExpression.contains("*") && !computableExpression.contains("/")) {
                            // We only execute the +/- operation if no point operation is left
                            if (operator.equals("+")) {
                                computableExpression.set(i, addition((double) computableExpression.get(i - 1), (double) computableExpression.get(i + 1)));
                            } else if (operator.equals("-")) {
                                computableExpression.set(i, subtraction((double) computableExpression.get(i - 1), (double) computableExpression.get(i + 1)));
                            }
                        } else {
                            // We have to continue here, no operation matched that means we should look at the next operation!
                            continue;
                        }
                        // First we remove the second value that we don't mess up with the lists order.
                        computableExpression.remove(i + 1);
                        // After removal of the second one we can remove the first one ;)
                        computableExpression.remove(i - 1);
                        break;
                    }
                }
                return new Expression(computableExpression).calculate();
            }
            return (double) computableExpression.get(0);
        } else {
            throw new InvalidExpression(expression);
        }
    }
}
