package models;

/**
 * Data model for Volume View.
 *
 * @author Cyrill Näf & Nicolas Stutz
 */
public class Volume {

    private static int height = 30;
    private static int length = 30;
    private static int depth = 30;
    private static int result = 0;

    /**
     * Calculates Volume with given parameters hight, length, depth.
     * No return value. Result must be taken with getResult.
     */
    public static void calculate() {
        result = (height * length * depth);
    }

    public static int getResult() {
        return Volume.result;
    }

    public static void setHeight(int height) {
        Volume.height = height;
    }

    public static void setLength(int length) {
        Volume.length = length;
    }

    public static void setDepth(int depth) {
        Volume.depth = depth;
    }
}