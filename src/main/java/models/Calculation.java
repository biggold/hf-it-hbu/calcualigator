package models;

/**
 * Table model describing a data row for a calculation.
 *
 * @author Cyrill Näf & Nicolas Stutz
 */
public class Calculation {
    private String expression;
    private double result;

    public Calculation(String expression, double result) {
        this.expression = expression;
        this.result = result;
    }

    public double getResult() {
        return result;
    }

    public String getExpression() {
        return expression;
    }
}
