package exceptions;

import java.util.List;

/**
 * Exception for invalid Operator.
 *
 * @author Cyrill Näf & Nicolas Stutz
 */
public class InvalidOperator extends Exception {
    public InvalidOperator(char operator, List<Character> allowedOperators) {
        super("Operator ( " + operator + " ) not allowed, allowed operators: " + allowedOperators.toString());
    }
}
