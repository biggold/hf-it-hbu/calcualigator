package exceptions;

import java.util.List;

/**
 * Exception for invalid Delimiter.
 *
 * @author Cyrill Näf & Nicolas Stutz
 */
public class InvalidDelimiter extends Exception {
    public InvalidDelimiter(char delimiter, List<Character> allowedDelimiters) {
        super("Delimiter ( " + delimiter + " ) not allowed, allowed operators: " + allowedDelimiters.toString());
    }
}
