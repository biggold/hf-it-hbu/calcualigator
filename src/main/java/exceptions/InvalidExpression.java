package exceptions;

/**
 * Exception for invalid Expression.
 *
 * @author Cyrill Näf & Nicolas Stutz
 */
public class InvalidExpression extends Exception {
    public InvalidExpression(String currentExpression) {
        super("Expression ( " + currentExpression + " ) is not valid!");
    }
}
