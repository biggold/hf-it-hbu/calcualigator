package exceptions;

/**
 * Exception for Value Creates Invalid Expression
 *
 * @author Cyrill Näf & Nicolas Stutz
 */
public class ValueCreatesInvalidExpression extends Exception {
    public ValueCreatesInvalidExpression(char addedChar, String currentExpression) {
        super("Cant add character ( " + addedChar + " ) to the already existing expression ( " + currentExpression + " ).");
    }
}
