import controllers.HistoryController;
import ui.views.MainWindow;

/**
 * Main class, CalcuAligator, the snappy calculator!
 *
 * @author Cyrill Näf & Nicolas Stutz
 */
public final class main {
    public static void main(String[] args) {

        HistoryController.createFile();
        MainWindow mainWindow = new MainWindow();
    }
}
