package controllers;

import models.Volume;
import ui.views.VolumePanel;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * This class defines the logic for the Volume Calculation
 *
 * @author Cyrill Näf & Nicolas Stutz
 */
public class VolumeController implements ActionListener, ChangeListener {
    private JButton btn;
    private JSlider source;
    private JTextField result;


    /**
     * Constructor when initializing as Button
     *
     * @param btn JButton
     */
    public VolumeController(JButton btn) {
        this.btn = btn;
    }

    /**
     * Constructor when initializing as Slider
     *
     * @param source JSlider
     * @param result JTextField
     */
    public VolumeController(JSlider source, JTextField result) {
        this.source = source;
        this.result = result;
    }


    /**
     * Depending on CalcButton.setText of the button given as parameter set action will be executed.
     *
     * @param event ActionEvent
     */
    @Override
    public void actionPerformed(ActionEvent event) {
        if ("Clear".equals(btn.getText())) {
            VolumePanel.result.setText("0");
        }
    }

    /**
     * Switch statement sets the Value of the Slider according to its Name Tag,
     * followed by calculation of the volume and setting the result as text in respective TextField.
     *
     * @param event ActionEvent
     */
    @Override
    public void stateChanged(ChangeEvent event) {
        switch (source.getName()) {
            case "height" -> Volume.setHeight(source.getValue());
            case "length" -> Volume.setLength(source.getValue());
            case "depth" -> Volume.setDepth(source.getValue());
        }
        Volume.calculate();
        result.setText(((Integer) Volume.getResult()).toString());
    }
}

