package controllers;

import models.Calculation;
import models.Expression;
import ui.views.HistoryPanel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystem;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Controller behind the History View.
 *
 * @author Cyrill Näf & Nicolas Stutz
 */
public class HistoryController implements ActionListener {
    private JButton btn;
    //File and Path to File definition
    static FileSystem fs = FileSystems.getDefault();
    static String fsSep = fs.getSeparator();
    static Path filePath = Paths.get("." + fsSep + "src" + fsSep + "main" + fsSep + "resources" + fsSep + "output.xml");

    /**
     * Constructor when initializing as Button
     *
     * @param btn JButton
     */
    public HistoryController(JButton btn) {
        this.btn = btn;
    }


    /**
     * Creats file output.xml if it doesn't exist yet.
     */
    public static void createFile() {
        if (!Files.exists(filePath)) {
            try {
                Files.createFile(filePath);
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    /**
     * Creats new file output.xml. Used for Clear History button.
     */
    public static void createNewFile() {
        try {
            Files.deleteIfExists(filePath);
            Files.createFile(filePath);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Returns initial Data from output.xml. Used in History View.
     */
    public static void init() {
        List<Calculation> calculations = readLines();
        for (Calculation calculation : calculations) {
            addCalculation(calculation);
        }
    }

    /**
     * Reads all lines from output.xml
     */
    private static List<Calculation> readLines() {
        List<Calculation> calculations = new ArrayList<Calculation>();
        try {
            BufferedReader br = Files.newBufferedReader(filePath, StandardCharsets.UTF_8);
            String readLine;
            while ((readLine = br.readLine()) != null) {
                String[] arrOfStr = readLine.split(";");
                // We only care about correct lines
                if (arrOfStr.length == 2) {
                    Calculation calculation = new Calculation(arrOfStr[0], Double.parseDouble(arrOfStr[1]));
                    calculations.add(calculation);
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return calculations;
    }

    /**
     * Writes new lines to output.xml
     */
    private static void writeLine(Calculation calculation) {
        try {
            FileWriter fw = new FileWriter(filePath.toString(), true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter pw = new PrintWriter(bw);
            // Write calculation as ; seperated value :)
            pw.println(calculation.getExpression() + ";" + calculation.getResult());
            pw.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }


    /**
     * Adds calculation to History view and output.xml.
     *
     * @param expression Expression object
     * @param result     Result as double
     */
    public static void addCalculation(Expression expression, Double result) {
        Calculation calculation = new Calculation(expression.getExpression(), result);
        writeLine(calculation);
        addCalculation(calculation);
    }

    /**
     * Adds Calculation to History Panel.
     *
     * @param calculation
     */
    private static void addCalculation(Calculation calculation) {
        HistoryPanel.tableData.addCalculation(calculation);
    }


    /**
     * Depending on CalcButton.setText of the button given as parameter set action will be executed.
     *
     * @param event ActionEvent
     */
    @Override
    public void actionPerformed(ActionEvent event) {
        if ("Clear History".equals(btn.getText())) {
            createNewFile();
            HistoryPanel.tableData.removeAllCalculations();
        }
    }
}
