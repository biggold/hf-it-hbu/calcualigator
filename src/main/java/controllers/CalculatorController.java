package controllers;

import models.Expression;
import ui.views.CalculatorPanel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Controller behind the Calculator View.
 *
 * @author Cyrill Näf & Nicolas Stutz
 */
public class CalculatorController implements ActionListener {
    private JButton btn;
    private static Expression expression = new Expression();

    public CalculatorController(JButton btn) {
        this.btn = btn;
    }

    /**
     * Depending on CalcButton.setText of the button given as parameter set action will be executed.
     */
    @Override
    public void actionPerformed(ActionEvent event) {
        try {
            String action = event.getActionCommand();
            try {
                int number = Integer.parseInt(action);
                expression.addNumber(number);
            } catch (NumberFormatException nfe) {
                char tmpChar = action.toCharArray()[0];
                if (Expression.getAllowedOperators().contains(tmpChar)) {
                    expression.addOperator(tmpChar);
                } else if (Expression.getAllowedDelimiters().contains(tmpChar)) {
                    expression.addDelimiter(tmpChar);
                } else if (tmpChar == '=') {
                    Double result = expression.calculate();
                    HistoryController.addCalculation(expression, result);
                    expression = new Expression(result);
                } else if (tmpChar == 'C' || action.equals("CE")) {
                    expression = new Expression();
                    CalculatorPanel.textField.setText("");
                }
            } finally {
                CalculatorPanel.textField.setText(expression.getExpression());
            }
        } catch (Exception e) {
            CalculatorPanel.textField.setText(e.getMessage());
        }
    }
}
