package ui.views;

import controllers.VolumeController;
import ui.components.Labels;
import ui.components.VolumeButton;

import javax.swing.*;
import java.awt.*;


/**
 * Volume panel View.
 *
 * @author Cyrill Näf & Nicolas Stutz
 */
public class VolumePanel extends JPanel {

    public static JTextField result = new JTextField("");

    public VolumePanel(){
        this.setBounds(330,40,310,250);
        this.setBorder(BorderFactory.createLineBorder(Color.darkGray, 3));
        this.setLayout(new GridBagLayout());
        this.setToolTipText("Volume Panel");
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(4,4,4,4);

        result.setToolTipText("Volume calculation result");
        result.setEditable(false);
        result.setHorizontalAlignment(SwingConstants.RIGHT);


        /**
         * Component definition
         */
        JSlider height = new JSlider(0,60,30);
        height.setToolTipText("Hight Slider");
        height.setName("height");
        height.setMajorTickSpacing(10);
        height.setMinorTickSpacing(5);
        height.setPaintTicks(true);
        height.setPaintLabels(true);
        height.addChangeListener(new VolumeController(height,result));

        JSlider length = new JSlider(0,60,30);
        length.setToolTipText("Length Slider");
        length.setName("length");
        length.setMajorTickSpacing(10);
        length.setMinorTickSpacing(5);
        length.setPaintTicks(true);
        length.setPaintLabels(true);
        length.addChangeListener(new VolumeController(length,result));

        JSlider depth = new JSlider(0,60,30);
        depth.setToolTipText("Depth Slider");
        depth.setName("depth");
        depth.setMajorTickSpacing(10);
        depth.setMinorTickSpacing(5);
        depth.setPaintTicks(true);
        depth.setPaintLabels(true);
        depth.addChangeListener(new VolumeController(depth,result));

        JButton clearButton = new VolumeButton("Clear");
        clearButton.setToolTipText("Cleares Volume result");

        /**
         * Components inizialisation
         */
        c.gridx = 0;
        c.gridy = 1;
        this.add(new Labels("Height"),c);
        c.gridx = 1;
        c.gridwidth = 2;
        this.add(height,c);
        c.gridx = 3;
        c.gridwidth = 1;
        this.add(new Labels("cm"),c);

        c.gridx = 0;
        c.gridy = 2;
        this.add(new Labels("Length"),c);
        c.gridx = 1;
        c.gridwidth = 2;
        this.add(length,c);
        c.gridx = 3;
        c.gridwidth = 1;
        this.add(new Labels("cm"),c);

        c.gridx = 0;
        c.gridy = 3;
        this.add(new Labels("Depth"),c);
        c.gridx = 1;
        c.gridwidth = 2;
        this.add(depth,c);
        c.gridx = 3;
        c.gridwidth = 1;
        this.add(new Labels("cm"),c);

        c.gridx = 0;
        c.gridy = 4;
        this.add(new Labels("Result", "Displays result in cm^3"),c);
        c.gridx = 1;
        c.gridwidth = 2;
        this.add(result,c);
        c.gridx = 3;
        c.gridwidth = 1;
        this.add(new Labels("cm^3"),c);

        c.gridx = 1;
        c.gridy = 5;
        c.gridwidth = 2;
        this.add(clearButton,c);
    }
}
