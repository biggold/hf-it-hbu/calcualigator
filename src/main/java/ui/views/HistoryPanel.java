package ui.views;

import controllers.HistoryController;
import helpers.LineWrapCellRenderer;
import models.History;

import javax.swing.*;
import java.awt.*;

/**
 * History panel View.
 *
 * @author Cyrill Näf & Nicolas Stutz
 */
public class HistoryPanel extends JPanel {
    public static History tableData = new History();
    public static JTable table = new JTable(tableData);
    public static JScrollPane historyDisplay = new JScrollPane(table);

    public HistoryPanel() {
        this.setBounds(10, 310, 630, 280);
        this.setBorder(BorderFactory.createLineBorder(Color.darkGray, 3));
        this.setToolTipText("History Panel");

        historyDisplay.setPreferredSize(new Dimension(610, 260));
        historyDisplay.setToolTipText("ScrollPane makes the History Scrolable");

        /**
         * Table settings.
         */
        table.setShowGrid(false);
        table.setFocusable(false);
        table.setRowSelectionAllowed(false);
        table.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        table.setToolTipText("Displays history of Calculated expressions");
        table.setShowGrid(true);

        /**
         * Settings for Column 1 (is the result column)
         */
        table.getColumnModel().getColumn(1).setMinWidth(100);
        table.getColumnModel().getColumn(1).setPreferredWidth(200);
        table.getColumnModel().getColumn(1).setMaxWidth(250);

        /**
         * Settings for Column 0 (is the expression column)
         */
        table.getColumnModel().getColumn(0).setCellRenderer(new LineWrapCellRenderer());

        /**
         * Read initial data.
         */
        HistoryController.init();

        this.add(historyDisplay);
    }
}