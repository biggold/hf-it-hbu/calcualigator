package ui.views;

import ui.components.CalcButton;

import javax.swing.*;
import java.awt.*;

/**
 * Calculater panel View.
 *
 * @author Cyrill Näf & Nicolas Stutz
 */
public class CalculatorPanel extends JPanel {
    public static JTextField textField = new JTextField();

    public CalculatorPanel() {

        this.setBounds(10, 40, 310, 250);
        this.setBorder(BorderFactory.createLineBorder(Color.darkGray, 3));
        this.setLayout(new GridBagLayout());
        this.setToolTipText("Calculater Panel");
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.insets = new Insets(1, 1, 1, 1);
        c.weightx = 100;
        c.weighty = 100;

        /**
         * Displays output.
         */
        textField.setToolTipText("Calculaters result");
        textField.setEditable(false);
        textField.setHorizontalAlignment(SwingConstants.RIGHT);

        /**
         * Buttons
         */
        JButton b1 = new CalcButton("1");
        JButton b2 = new CalcButton("2");
        JButton b3 = new CalcButton("3");
        JButton b4 = new CalcButton("4");
        JButton b5 = new CalcButton("5");
        JButton b6 = new CalcButton("6");
        JButton b7 = new CalcButton("7");
        JButton b8 = new CalcButton("8");
        JButton b9 = new CalcButton("9");
        JButton b0 = new CalcButton("0");
        JButton mult = new CalcButton("*");
        mult.setToolTipText("Mltiply");
        JButton div = new CalcButton("/");
        div.setToolTipText("Devide");
        JButton plus = new CalcButton("+");
        plus.setToolTipText("Add");
        JButton min = new CalcButton("-");
        min.setToolTipText("Reduce");
        JButton equ = new CalcButton("=");
        equ.setToolTipText("Calculate");
        JButton period = new CalcButton(".");
        period.setToolTipText("Make the number floating");
        JButton clear = new CalcButton("C");
        clear.setToolTipText("Cleares result");

        /**
         * Components inizialisation
         */
        c.gridx = 0;
        c.gridy = 1;
        c.gridwidth = 4;
        this.add(textField, c);

        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = 2;
        this.add(mult, c);
        c.gridx = 1;
        this.add(div, c);
        c.gridx = 2;
        this.add(plus, c);
        c.gridx = 3;
        this.add(min, c);


        c.gridx = 0;
        c.gridy = 3;
        this.add(b7, c);
        c.gridx = 1;
        this.add(b8, c);
        c.gridx = 2;
        this.add(b9, c);

        c.gridheight = 4;
        c.gridx = 3;
        c.gridy = 3;
        this.add(equ, c);

        c.gridheight = 1;
        c.gridx = 0;
        c.gridy = 4;
        this.add(b4, c);
        c.gridx = 1;
        this.add(b5, c);
        c.gridx = 2;
        this.add(b6, c);

        c.gridx = 0;
        c.gridy = 5;
        this.add(b1, c);
        c.gridx = 1;
        this.add(b2, c);
        c.gridx = 2;
        this.add(b3, c);

        c.gridx = 0;
        c.gridy = 6;
        this.add(period, c);

        c.gridx = 1;
        this.add(b0, c);

        c.gridx = 2;
        this.add(clear, c);
    }
}
