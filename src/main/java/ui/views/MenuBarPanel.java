package ui.views;

import ui.components.CalcButton;
import ui.components.HistoryButton;

import javax.swing.*;

/**
 * Menu bar View.
 *
 * @author Cyrill Näf & Nicolas Stutz
 */
public class MenuBarPanel extends JMenuBar {

    public MenuBarPanel() {
        this.setBounds(0, 0, 700, 20);
        this.setToolTipText("Menu Bar");

        /**
         * Component definition
         */
        JMenu menu = new JMenu("Datei");
        menu.setToolTipText("Menu");
        JButton clearButton = new HistoryButton("Clear History");
        clearButton.setToolTipText("Clears history");

        /**
         * Components inizialisation
         */
        menu.add(clearButton);
        this.add(menu);
    }
}
