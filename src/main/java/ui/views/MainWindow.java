package ui.views;


import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * Main window view
 *
 * @author Cyrill Näf & Nicolas Stutz
 */
public class MainWindow extends JFrame {

    public MainWindow() {
        /**
         * Window settings
         */
        this.setTitle("CalcuAligator");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);
        this.setResizable(false);
        this.setSize(665, 650);
        this.setLocation(800, 200);
        this.getContentPane().setBackground(new Color(179, 239, 203));

        /**
         * Titel Calculator
         */
        JLabel titleCalc = new JLabel("Calculator");
        titleCalc.setBounds(10, 25, 100, 10);
        titleCalc.setToolTipText("Calculator Titel");

        /**
         * Titel Volume
         */
        JLabel titleVolu = new JLabel("Volume");
        titleVolu.setBounds(330, 25, 100, 10);
        titleVolu.setToolTipText("Volume Titel");
        /**
         * Titel Tooltip
         */
        JLabel titleTT = new JLabel("History");
        titleTT.setBounds(10, 290, 580, 20);
        titleTT.setToolTipText("History Titel");

        /**
         * Credits
         */
        JLabel credits = new JLabel("MLZ JA2 by Cyrill Näf, Nicolas Stutz");
        credits.setBounds(10, 590, 620, 20);
        credits.setToolTipText("Credits of CalcuAligator");

        /**
         * Components inizialisation
         */
        this.add(titleTT);
        this.add(titleCalc);
        this.add(titleVolu);
        this.add(new MenuBarPanel());
        this.add(new CalculatorPanel());
        this.add(new VolumePanel());
        this.add(new HistoryPanel());
        this.add(credits);

        /**
         * Sets Icon of MainWindow
         */
        try {
            setIconImage(ImageIO.read(new File("./src/main/resources/logo.png")));
        } catch (IOException e) {
            e.printStackTrace();
        }

        /**
         * Sets View visible
         */
        this.setVisible(true);
    }
}
