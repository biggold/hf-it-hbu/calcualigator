package ui.components;

import controllers.CalculatorController;
import org.w3c.dom.Text;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * Prepares JButton with all necessities for CalcButton.
 *
 * @author Cyrill Näf & Nicolas Stutz
 */
public class CalcButton extends JButton {

    public CalcButton(String text) {
        this.setText(text);
        this.setFocusable(false);
        this.addActionListener(new CalculatorController(this));
        this.setToolTipText("Button Nr " + text);
    }

}
