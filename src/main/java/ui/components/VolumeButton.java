package ui.components;


import controllers.VolumeController;

import javax.swing.*;

/**
 * Prepares JButton with all necessities for VolumeButton.
 *
 * @author Cyrill Näf & Nicolas Stutz
 */
public class VolumeButton extends JButton {

    public VolumeButton(String text) {
        this.setText(text);
        this.setFocusable(false);
        this.addActionListener(new VolumeController(this));
    }

}
