package ui.components;


import controllers.HistoryController;

import javax.swing.*;

/**
 * Prepares JButton with all necessities for HistoryButton.
 *
 * @author Cyrill Näf & Nicolas Stutz
 */
public class HistoryButton extends JButton {
    public HistoryButton(String text) {
        this.setText(text);
        this.setFocusable(false);
        this.addActionListener(new HistoryController(this));
    }
}
