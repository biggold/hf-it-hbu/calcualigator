package ui.components;

import javax.swing.*;

/**
 * Prepares JLabel with all necessities for Labels.
 *
 * @author Cyrill Näf & Nicolas Stutz
 */
public class Labels extends JLabel {

    public Labels(String text){
        this.setText(text);
        this.setToolTipText(text);
    }

    public Labels(String text, String toolTip){
        this.setText(text);
        this.setToolTipText(toolTip);
    }
}
