package helpers;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.util.Arrays;

/**
 * Helper class, sets row height of each expression in historyPanel.
 *
 * @author Cyrill Näf & Nicolas Stutz
 */
public class LineWrapCellRenderer extends JTextArea implements TableCellRenderer {

    /**
     * Custom Table cell renderer. Supports dinamic row height and line wraps.
     *
     * @param table      Current table instance
     * @param value      Data for field value
     * @param isSelected
     * @param hasFocus
     * @param row        Row ID
     * @param column     Column ID
     * @return
     */
    @Override
    public Component getTableCellRendererComponent(
            JTable table,
            Object value,
            boolean isSelected,
            boolean hasFocus,
            int row,
            int column) {
        this.setFont(new Font(Font.MONOSPACED, Font.PLAIN, this.getFont().getSize()));
        this.setText((String) value);
        this.setWrapStyleWord(true);
        this.setLineWrap(true);
        this.setSize(new Dimension(table.getColumnModel().getColumn(column).getWidth(), this.getPreferredSize().height));
        table.setRowHeight(row, this.getPreferredSize().height);
        return this;
    }

}
